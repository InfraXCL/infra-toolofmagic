FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN apt-get update && apt-get -y install libldap2-dev libsasl2-dev python-dev libssl-dev
RUN pip install -r requirements.txt
RUN mkdir /code/storage
RUN curl http://infratools.ovh.xcl.ie/ipa/ca.crt > /usr/local/share/ca-certificates/ipaca.crt
RUN update-ca-certificates
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/
ADD . /code/
