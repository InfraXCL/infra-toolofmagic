from django.urls import include,path,re_path
from django.views.decorators.csrf import csrf_exempt
from web.models import Module
from . import views



urlpatterns = [
    # path('', include(rest.urls))
    # re_path(r'^ipa/', csrf_exempt(views.handle_request), name='router')
]

modules = Module.objects.all()
for module in modules:
    urlpatterns.append(re_path(r'^'+module.name+'/', csrf_exempt(views.handle_request), name='router'))

