fieldmap = {
    'str': 'string',
    'int': 'number',
    'bool': 'boolean'
}

def generate(fieldtype):
    return fieldmap[fieldtype]