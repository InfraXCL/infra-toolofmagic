from django.http import JsonResponse
from modules.router import Router
from web.tools import weblogger


def handle_request(request):

    weblogger.write_log(request.user, request.method, request.path, request.body)

    response = {"success": False}

    uri = request.get_full_path().split('/')
    del uri[0:3]
    del uri[(len(uri) - 1)]
    print(uri)

    module_name = uri[0]
    del uri[0]
    function_name = ''

    for segment in uri:
        if function_name == '':
            function_name += segment
        else:
            function_name += '_' + segment

    if request.method == "GET":
        print('Router got a GET: '+module_name)
        response = Router().get_form(module_name, function_name)

    elif request.method == "POST":
        print('Router got a POST: ' + module_name)
        print(request.body)

        response = Router().run(module_name, function_name, request.body)

    return JsonResponse(response)
