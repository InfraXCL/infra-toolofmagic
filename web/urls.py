from django.urls import include,path,re_path
from django.views.decorators.csrf import csrf_exempt
from . import views

urlpatterns = [
    path('', views.start_web, name='start_web'),
    re_path(r'^login', views._login, name='login'),
    re_path(r'^logout', views._logout, name='logout'),
    path('apis/', views.api_explorer, name='api_explorer'),
    re_path(r'^apis/', views.api_view, name='api_views'),
    path('logs/', views.logs, name='logs'),
]