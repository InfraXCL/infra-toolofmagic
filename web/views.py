from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.shortcuts import redirect
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from .models import WebSection, WebLog
from modules.router import Router
from web.tools import weblogger


def _login(request):

    if request.method == "GET":
        template = loader.get_template('main/login.html')
        context = {

        }
        return HttpResponse(template.render(context, request))

    elif request.method == "POST":

        if request.POST['username'] is not None and request.POST['password'] is not None:
            weblogger.write_log('unknown', request.method, request.path, 'username=' + request.POST['username'])
            user = authenticate(username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                print('Logged in as '+request.POST['username'])
                login(request, user)
                return JsonResponse({'success': True})
            else:
                print('ERROR: Bad login for '+request.POST['username'])
                return JsonResponse({'success': False})


def _logout(request):

    weblogger.write_log(request.user, request.method, request.path, request.body)
    
    logout(request)
    template = loader.get_template('main/login.html')
    context = {

    }
    return HttpResponse(template.render(context, request))


def start_web(request):

    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    weblogger.write_log(request.user, request.method, request.path, request.body)

    template = loader.get_template('main/base_index.html')

    sections = WebSection.objects.filter(parent=0).order_by('order')
    for section in sections:
        section_children = WebSection.objects.filter(parent=section.id).order_by('order')
        section.children = section_children
    context = {
        'sections': sections
    }

    return HttpResponse(template.render(context, request))


def logs(request):
    template = loader.get_template('logs/logs.html')

    sections = WebSection.objects.filter(parent=0).order_by('order')
    for section in sections:
        section_children = WebSection.objects.filter(parent=section.id).order_by('order')
        section.children = section_children

    context = {

        'sections': sections,
        'logs': WebLog.objects.all(),
        'load_js': ['logs.js']
    }

    return HttpResponse(template.render(context, request))


def api_view(request):

    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    weblogger.write_log(request.user, request.method, request.path, request.body)

    template = loader.get_template('apis/apiview.html')

    sections = WebSection.objects.filter(parent=0).order_by('order')
    for section in sections:
        section_children = WebSection.objects.filter(parent=section.id).order_by('order')
        section.children = section_children

    target = request.get_full_path().split('/')

    target_path = '/api/v1/'+request.get_full_path().split("/", 2)[2]

    context = {
        'sections': sections,
        'target': target_path
    }

    return HttpResponse(template.render(context, request))


def api_explorer(request):

    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    weblogger.write_log(request.user, request.method, request.path, request.body)

    template = loader.get_template('apis/explorer.html')

    sections = WebSection.objects.filter(parent=0).order_by('order')
    for section in sections:
        section_children = WebSection.objects.filter(parent=section.id).order_by('order')
        section.children = section_children

    target = request.get_full_path().split('/')

    target_path = '/api/v1/'+request.get_full_path().split("/", 2)[2]

    context = {
        'sections': sections,
        'modules': Router().build_tree()
    }

    return HttpResponse(template.render(context, request))