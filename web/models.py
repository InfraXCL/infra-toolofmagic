from django.db import models
from django_mysql.models import JSONField, Model


class WebLog(Model):

    method = models.CharField(max_length=5)
    user = models.CharField(max_length=20)
    path = models.CharField(max_length=200)
    request_data = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)


class Module(Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    settings = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class WebSection(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    icon = models.CharField(max_length=20)
    order = models.IntegerField()
    parent = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


