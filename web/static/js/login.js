$(document).ready(function () {
    console.log('login ready!');


    $('#loginForm').submit(
        function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: '/login',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {
                    console.log(response);

                    if(response.success)
                    {
                        $("#loginForm").notify('Cool, we know who you are', "success", { position: "top left"});
                        if (getUrlParameter('next') != '' && getUrlParameter('next')!=null){
                            window.location.replace(getUrlParameter('next'))
                        }
                        else
                        {
                            window.location.replace('/')
                        }

                    }
                    else
                    {
                        $("#loginForm").notify('Wrong username or password!', "error", { position: "top left"});
                    }

                }
            });

        }
    )

});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};