$(document).ready(function () {
    console.log('main ready!');

    $('#side-menu').metisMenu();
    //$('#side-menu li').click(navClick);

    $('.metisFolder').metisMenu();

    $('#api_tree a').click(apiView2);




});

function apiView2(event) {
    event.preventDefault();
    console.log('clicked something in menu!');
    console.log(event.target.getAttribute('href'))

    if(event.target.getAttribute('href')!='#')
    {
        apiView(event.target.getAttribute('href'));
    }
}

function apiView(targetApi) {
    console.log(targetApi);
    $('#api_name').html(targetApi);
    $('#api_form').html('');
    $.get(targetApi, function (data) {
        console.log(data);
        $('#api_form').jsonForm({
            schema: data
            ,
            onSubmit: function (errors, values) {
                if (errors) {
                    $('#api_res').html('<p>I beg your pardon?</p>');
                }
                else {
                    $('#api_res').html('Request sent!<p><p>');
                    $.post(
                        targetApi,
                        JSON.stringify(values),
                        function (data) {
                            console.log('Nest je stiglo!');
                            console.log(data);
                            $('#api_res').append('Response:<p><pre>' + JSON.stringify(data, null, 2) + '</pre>');
                        },
                        'json'
                    );
                }
            }
        });
    }, 'json');
}



function navClick(event) {
    event.preventDefault();
    var as = $('a');
    var uls = $('ul');
    if ($(this).find(uls).length == 0) {
        history.pushState('data', '', $(this).find(as).attr('href'));
        $('#page-wrapper').load($(this).find(as).attr('href'));
    }


}




