from web.models import WebLog
import json


def write_log(user, method, path, request_data):

    new_log = WebLog()
    new_log.path = path
    new_log.user = user
    new_log.method = method
    new_log.request_data = json.dumps({
        'request': str(request_data)
                             })
    new_log.save()
