import inspect
import importlib
from modules.module import RestModule
from web.models import Module
from rest.tools import formgenerator

from django.conf import settings


class Router(object):

    def __init__(self):
        self.name = 'SuperHendler'
        self.loaded = {}

    def init_module(self, module_name, root='modules'):
        try:
            module = importlib.import_module('.'+module_name, root)
            for x in dir(module):
                # print(x)
                obj = getattr(module, x)
                # print(obj)
                if inspect.isclass(obj) and issubclass(obj, RestModule) and obj is not RestModule:
                    # print('Returning:' + str(obj))
                    # instance = obj(settings.MODULES_IPA_REST)
                    self.loaded[module_name] = obj
                    return True
        except ImportError:
            return False

    def get_module(self, module_name):
        if module_name in self.loaded:
            return self.loaded[module_name]
        else:
            if self.init_module(module_name):
                return self.loaded[module_name]

    def find_function(self, module_name, function_name):
        module = self.get_module(module_name)
        func = getattr(module, function_name)
        return func

    def get_form(self, module_name, function_name):
        module = self.get_module(module_name)
        fields = {}

        func = getattr(module, function_name)
        signature = inspect.signature(func)
        for param in signature.parameters:
            print(signature.parameters[str(param)])
            if str(param) != 'self':
                if inspect.isclass(signature.parameters[str(param)].annotation):

                    fields[str(param)] = {
                        'type': formgenerator.generate(signature.parameters[str(param)].annotation.__name__),
                        'required': True,
                        'title': str(param).capitalize()
                    }
                    if signature.parameters[str(param)].default is not None:
                        fields[str(param)]['default'] = signature.parameters[str(param)].default
                else:
                    fields[str(param)] = {
                        'type': formgenerator.generate(
                            signature.parameters[str(param)].annotation.__args__[0].__name__),
                        'required': False,
                        'title': str(param).capitalize()
                    }

        return fields


    def build_tree(self):
        modules = Module.objects.all()

        module_list = []

        for module in modules:

            list_member = {
                'name': module.name,
                'apis': self.build_module_tree(module.name)
            }
            module_list.append(list_member)
        print(module_list)
        return module_list

    # TODO Fix the tree so it doesn't ignore single functions in a subgroup
    def build_module_tree(self, module_name):
        module = self.get_module(module_name)
        function_array = []
        built_list = []
        for func in dir(module):
            if not func.startswith('_'):
                function_array.append(func.split('_'))

        for entry in function_array:

            entry_done = False

            for element in built_list:
                if element['name'] == entry[0]:
                    action_url = '/api/v1/{0}/{1}'.format(module_name, '/'.join(map(str, entry)) + '/')
                    if len(entry) == 2:
                        action_name = entry[1]
                    else:
                        trim = entry.copy()
                        del trim[0]
                        action_name = '/'.join(map(str, trim))
                    element['actions'].append({'url': action_url, 'name': action_name})
                    entry_done = True

            if not entry_done:
                item = dict()
                item['name'] = entry[0]
                item['actions'] = []
                action_url = '/api/v1/{0}/{1}'.format(module_name, '/'.join(map(str, entry)) + '/')
                if len(entry) == 2:
                    action_name = entry[1]
                else:
                    trim = entry.copy()
                    del trim[0]
                    action_name = '/'.join(map(str, trim))
                item['actions'].append({'url': action_url, 'name': action_name})

                built_list.append(item)

        return built_list

    def run(self, module_name, function_name, request_data):
        func = self.find_function(module_name, function_name)
        # instance = self.get_module(module_name)(settings.MODULES_IPA_REST)
        instance = self.get_module(module_name)(Module.objects.filter(name=module_name)[0].settings)
        return instance._run(func, request_data)
