from rest.tools import formgenerator
import inspect
import importlib
from django.conf import settings


class RestModule(object):

    def __init__(self, _settings):
        self._settings = _settings

    def _return_message(self, message, status='false'):
        response = {}
        response["success"] = status
        response["message"] = message
        return response