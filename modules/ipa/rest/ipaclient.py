# -[python-freeipa-json]-------------------------------------------------------
# This is a very basic quick and dirty way to communicate with FreeIPA/IdM
# without having to install their toolchain, also you do not have to rely on
# kerberos implementations in python.
#
# This sorry excuse for a module have 1 requirement outside of the python
# standard library:
# * requests
#
# Todo:
# - Pull in the rest of the FreeIPA methods
# - Fix the "API version not sent" message
# -----------------------------------------------------------------------------

########### Xanadu Notes ##########
###################################
# service_add HOWTO:
#
# We need to have a host registered already (host_add)
# As the host is probably not in IPA DNS, we need to set the 'force' flag to True
#
# to generate the principal name we need the following notation:
# service_name/hostname@realm
#
# a valid principal name would be
#  openvpn/hostname.xcl.ie
#
# @lovro
###########

import requests
import json
import logging
from typing import Optional
from modules.module import RestModule


class IpaClient(RestModule):

    def __init__(self, _settings):
        super().__init__(_settings)
        self.name = 'ipa'
        self.server = self._settings['server']
        self.sslverify = self._settings['sslverify']
        self.log = logging.getLogger(__name__)
        self.session = requests.Session()

    def _run(self, func, request_data):
        self._login(self._settings['username'], self._settings['password'])
        return self._translator(func(self, **json.loads(request_data)))

    def _translator(self, response):
        if response['result'] is None and response['error'] is not None:
            logging.warning('IPA translator')
            logging.warning(response)
            return super()._return_message(response['error']['message'])
        elif response['result']['summary'] is not None:
            logging.warning('IPA translator 2')
            logging.warning(response)
            return super()._return_message(response['result']['summary'], True)
        else:
            logging.warning('IPA translator 3')
            logging.warning(response)
            return super()._return_message(response['result'], True)

        pass

    def _login(self, user, password):
        rv = None
        ipaurl = 'https://{0}/ipa/session/login_password'.format(self.server)
        header = {'referer': ipaurl, 'Content-Type':
                  'application/x-www-form-urlencoded', 'Accept': 'text/plain'}
        login = {'user': user, 'password': password}
        rv = self.session.post(ipaurl, headers=header, data=login,
                               verify=self.sslverify)

        if rv.status_code != 200:
            self.log.warning('Failed to log {0} in to {1}'.format(
                user,
                self.server)
            )
            rv = None
        else:
            self.log.info('Successfully logged in as {0}'.format(user))
            # set login_user for use when changing password for self
            self.login_user = user
        return rv

    def _makeReq(self, pdict):
        results = None
        ipaurl = 'https://{0}/ipa'.format(self.server)
        session_url = '{0}/session/json'.format(ipaurl)
        header = {'referer': ipaurl, 'Content-Type': 'application/json',
                  'Accept': 'application/json'}

        data = {'id': 0, 'method': pdict['method'], 'params':
                [pdict['item'], pdict['params']]}

        self.log.debug('Making {0} request to {1}'.format(pdict['method'],
                        session_url))

        request = self.session.post(
                session_url, headers=header,
                data=json.dumps(data),
                verify=self.sslverify
        )
        results = request.json()

        return results

    def config_show(self):
        m = {'method': 'config_show', 'item': [None], 'params': {'all': True}}
        results = self._makeReq(m)

        return results

    def cert_show(self, serial:int=None,):
        m = {'method': 'cert_show', 'item': [serial], 'params':
             {'all': True,}}
        results = self._makeReq(m)

        return results

    def cert_request(self, csr: str=None, principal: str=None):
        m = {'method': 'cert_request', 'item': [csr], 'params':
             {'all': True, 'principal': principal}}
        results = self._makeReq(m)
        logging.warning('IPA CERT REQUEST!')
        logging.warning(results)
        return results

    def group_add(self, group:str=None, gidnumber: Optional[int] =None, description: Optional[str] =None):
        m = {'method': 'group_add',
             'item': [group],
             'params': {
                 'all': True,
                 'description': description
             }
        }
        if gidnumber is not None:
            m['params']['gidnumber'] = gidnumber
        results = self._makeReq(m)

        return results

    def group_member_add(self, group:str=None, item:str=None, membertype:str=None):
        if membertype not in ['user', 'group']:
            raise ValueError('Type {0} is not a valid member type,\
             specify user or group'.format(membertype))
        m = {
                'item': [group],
                'method': 'group_add_member',
                'params': {
                    'all': True,
                    'raw': True,
                    membertype: item
                }
        }
        results = self._makeReq(m)

        return results

    def group_member_remove(self, group:str=None, items:str=None):
        if isinstance(items, str):
            items = [items]
        m = {
            "method": "group_remove_member",
            "item": [group],
            "params": {
                "all": False,
                "no_members": False,
                "raw": False,
                "user": items,
                "version": "2.164"
            }
        }
        results = self._makeReq(m)

        return results

    def group_find(self, group:str=None, sizelimit=40000):
        m = {'method': 'group_find', 'item': [group], 'params': {'all': True,
             'sizelimit': sizelimit}}
        results = self._makeReq(m)

        return results

    def group_show(self, group:str=None):
        m = {'item': [group], 'method': 'group_show', 'params':
             {'all': True, 'raw': False}}
        results = self._makeReq(m)

        return results

    def group_mod(self, group:str=None, addattrs=[], setattrs=[], delattrs=[]):
        m = {
            'method': 'group_mod',
            'item': [group],
            'params': {
                    'all': False,
                    'no_members': False,
                    'raw': False,
                    'rights': False,
                    'version': '2.164'
            }
        }
        if len(addattrs):
            m['params']['addattr'] = addattrs
        if len(setattrs):
            m['params']['setattr'] = setattrs
        if len(delattrs):
            m['params']['delattr'] = delattrs

        return self._makeReq(m)

    def host_add(self, hostname:str=None, opasswd: Optional[str]=None, force: bool=True):
        m = {'item': [hostname], 'method': 'host_add', 'params': {'all': True,
             'force': force, 'userpassword': opasswd}}
        results = self._makeReq(m)

        return results

    def host_del(self, hostname:str=None):
        m = {'item': [hostname], 'method': 'host_del', 'params': {}}
        results = self._makeReq(m)

        return results

    def host_find(self, hostname:str=None, in_hostgroup: Optional[str]= None, sizelimit=40000):
        m = {'method': 'host_find', 'item': [hostname], 'params':
             {'all': True, 'in_hostgroup': in_hostgroup, 'sizelimit': sizelimit}}
        results = self._makeReq(m)

        return results

    def host_mod(self, hostname:str=None, description:str=None, locality:str=None,
                 location=None, platform=None, osver=None):
        m = {'item': [hostname], 'method': 'host_mod', 'params':
             {'all': True, 'description': description, 'locality': locality,
              'nshostlocation': location, 'nshardwareplatform': platform,
              'nsosversion': osver}}
        results = self._makeReq(m)

        return results

    def host_show(self, hostname: str=None):
        m = {'item': [hostname], 'method': 'host_show', 'params':
             {'all': True}}
        results = self._makeReq(m)

        return results

    def hostgroup_add(self, hostgroup: str=None, description: Optional[str]=None):
        m = {
                'method': 'hostgroup_add',
                'item': [hostgroup],
                'params': {
                    'all': True,
                    'description': description
                }
        }
        results = self._makeReq(m)

        return results

    def hostgroup_member_add(self, hostgroup:str=None, hostname:str=None):
        if type(hostname) != list:
            hostname = [hostname]
        m = {
                'method': 'hostgroup_add_member',
                'item': [hostgroup],
                'params': {'host': hostname, 'all': True}
        }
        results = self._makeReq(m)

        return results

    def hostgroup_show(self, hostgroup:str=None):
        m = {'item': [hostgroup], 'method': 'hostgroup_show', 'params':
             {'all': True}}
        results = self._makeReq(m)
        return results

    def service_add(self, principalname:str=None):

        m = {'item': [principalname], 'method': 'service_add', 'params':
            {'force': True}}
        results = self._makeReq(m)

        return results

    def service_find(self, man_by_host:str=None, sizelimit=40000):

        m = {'item': [man_by_host], 'method': 'service_find', 'params':
            {'all': True, 'sizelimit': sizelimit}}
        results = self._makeReq(m)

        return results

    def user_add(self, user:str=None, opts:Optional[str]=None):
        opts['all'] = True
        m = {'method': 'user_add', 'item': [user], 'params': opts}
        results = self._makeReq(m)

        return results

    def user_find(self, user=None, attrs={}, sizelimit=40000):
        params = {'all': True,
                  'no_members': False,
                  'sizelimit': sizelimit,
                  'whoami': False}
        params.update(attrs)
        m = {'item': [user], 'method': 'user_find', 'params': params}
        results = self._makeReq(m)

        return results

    def user_show(self, user:str=None):
        m = {'item': [user], 'method': 'user_show', 'params':
             {'all': True, 'raw': False}}
        results = self._makeReq(m)

        return results

    def user_status(self, user:str=None):
        m = {'item': [user], 'method': 'user_status', 'params':
             {'all': True, 'raw': False}}
        results = self._makeReq(m)

        return results

    def user_unlock(self, user:str=None):
        m = {'item': [user], 'method': 'user_unlock', 'params':
             {'version': '2.112'}}
        results = self._makeReq(m)

        return results

    def user_disable(self, user:str=None):
        m = {'item': [user], 'method': 'user_disable', 'params':
             {'version': '2.112'}}
        results = self._makeReq(m)

        return results

    def user_mod(self, user: str=None, addattrs=[], setattrs=[], delattrs=[]):
        m = {
            'method': 'user_mod',
            'item': [user],
            'params': {
                    'all': False,
                    'no_members': False,
                    'raw': False,
                    'rights': False,
                    'version': '2.164'
            }
        }
        if len(addattrs):
            m['params']['addattr'] = addattrs
        if len(setattrs):
            m['params']['setattr'] = setattrs
        if len(delattrs):
            m['params']['delattr'] = delattrs

        return self._makeReq(m)

    def user_password_change(self, principal:str=None, passwd:str=None):
        item = [principal, passwd]
        if not principal.split('@')[0] == self.login_user:
            item.append('CHANGING_PASSWORD_FOR_ANOTHER_USER')
        m = {'method': 'passwd', 'params': {'version': '2.112'}, 'item': item}
        results = self._makeReq(m)

        return results


    def user_del(self, user:str=None, preserve: Optional[str]=True):
        m = {
            "item": [user],
            "method": "user_del",
            "params": {
                "continue": False,
                "preserve": preserve,
                "version": "2.164"
            }
        }

        return self._makeReq(m)

    def stageuser_find(self, user=None, attrs={}, sizelimit=40000):
        params = {'all': True,
                  'no_members': False,
                  'sizelimit': sizelimit,
        }
        params.update(attrs)
        m = {'item': [user], 'method': 'stageuser_find', 'params': params}
        results = self._makeReq(m)

        return results

    def stageuser_add(self, user, opts, addattrs=None, setattrs=None):
        opts['all'] = False
        if addattrs is not None:
            opts['addattr'] = addattrs
        if setattrs is not None:
            opts['setattr'] = setattrs
        m = {
            'method': 'stageuser_add',
            'item': [user],
            'params': opts
        }
        results = self._makeReq(m)

        return results

    def stageuser_del(self, user):
        m = {
            'method': 'stageuser_del',
            'item': [user],
            'params': {
                'version': '2.164'
            }
        }
        results = self._makeReq(m)

        return results

    def stageuser_mod(self, user, addattrs=[], setattrs=[], delattrs=[]):
        m = {
            'method': 'stageuser_mod',
            'item': [user],
            'params': {
                    'all': False,
                    'no_members': False,
                    'raw': False,
                    'rights': False,
                    'version': '2.164'
            }
        }
        if len(addattrs):
            m['params']['addattr'] = addattrs
        if len(setattrs):
            m['params']['setattr'] = setattrs
        if len(delattrs):
            m['params']['delattr'] = delattrs

        return self._makeReq(m)

    def stageuser_activate(self, user):
        m = {
            'method': 'stageuser_activate',
            'item': [user],
            'params': {
                'version': '2.164'
            }
        }
        results = self._makeReq(m)

        return results

    def selfservice_add(self, aciname, attrs, permissions=None):
        m = {
            'method': 'selfservice_add',
            'item': [aciname],
            'params': {
                'attrs': attrs,
                'all': True,
                'raw': False,
                'version': '2.164'
            }
        }
        if permissions is not None:
            m['params']['permissions'] = permissions
        results = self._makeReq(m)

        return results

    def automember_add(self, name:str =None, description: Optional[str] ='', type: Optional[str] ='group'):
        m = {
            'method': 'automember_add',
            'item': [name],
            'params': {
                'type': type,
                'all': True,
                'raw': False,
                'version': '2.164'
            }
        }
        if description:
            m['params']['description'] = description
        results = self._makeReq(m)

        return results

    def automember_condition_add(self, name:str =None, key:str =None, type:str =None, description='', inclusive_regex:Optional[str] ='', exclusive_regex: Optional[str] =''):
        m = {
            'method': 'automember_add_condition',
            'item': [name],
            'params': {
                'key': key,
                'type': type,
                'all': True,
                'raw': False,
                'version': '2.164'
            }
        }
        if inclusive_regex:
            m['params']['automemberinclusiveregex'] = inclusive_regex
        if exclusive_regex:
            m['params']['automemberexclusiveregex'] = exclusive_regex
        results = self._makeReq(m)

        return results
