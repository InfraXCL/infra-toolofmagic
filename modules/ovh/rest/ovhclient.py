import json
import logging
import ovh
from typing import Optional
from modules.module import RestModule


class OvhClient(RestModule):
    _client = None

    def __init__(self, _settings):
        super().__init__(_settings)
        self.name = 'ovh'
        self._client = self._get_client(_settings)

    def _get_client(self, settings=None):
        if self._client is None and settings is not None:
            self._client = ovh.Client(**settings)
            return self._client
        else:
            return self._client

    def _make_query(self):
        pass

    def _run(self, func, request_data):
        self._get_client()

        try:
            return self._return_message(func(self, **json.loads(request_data)), True)
        except Exception as e:
            return self._return_message(str(e))

    def _translator(self, response):
        print(response)
        return super()._return_message(response)

    def server_list(self):
        return self._client.get('/dedicated/server')

    def server_reverse_add(self, servername: str = None, reverse: str = None):
        server_ip = self.server_show(servername)['ip']
        return self._client.post('/ip/'+server_ip+'/reverse', ipReverse=server_ip, reverse=reverse)

    def server_show(self, servername: str = None):
        server_info = self._client.get('/dedicated/server/' + servername)
        return server_info

    def server_details_show(self, servername: str = None):
        server_info = self._client.get('/dedicated/server/' + servername)
        server_info['hardware'] = self._client.get('/dedicated/server/' + servername + '/specifications/hardware')
        return server_info

    def server_install(self, servername: str=None, hostname: str=None, templatename: str=None):
        details = {}
        details['customHostname'] = hostname
        return self._client.post('/dedicated/server/'+servername+'/install/start', templateName=templatename, details=details)

    def server_interface_get(self, servername: str = None, interfacetype: str = 'vrack'):
        interface_list = self._client.get('/dedicated/server/' + servername + '/virtualNetworkInterface',
                                          mode=interfacetype,
                                          )
        return interface_list

    def server_reboot(self, servername: str=None):
        return self._client.post('/dedicated/server/' + servername + '/reboot')

    def vrack_list(self):
        return self._client.get('/vrack')

    def vrack_show(self, vrackname: str = None):

        vracks = self.vrack_list()
        if vrackname in vracks:
            return self._client.get('/vrack/' + vrackname)

        else:
            for vrack in vracks:
                res = self._client.get('/vrack/' + vrack)
                if res['name'].lower() == vrackname.lower():
                    res['id'] = vrack
                    return res

        # return self._client.get('/vrack/' + vrackname)

    def vrack_server_add(self, servername: str = None, vrackname: str = None):

        if vrackname.startswith('pn'):
            vrack = vrackname
        else:
            vrack = self.vrack_show(vrackname)['id']

        interface = self.server_interface_get(servername)

        if len(interface) == 0:
            print('Don\'t have interfeces')
            result = self._client.post('/vrack/' + vrack + '/dedicatedServer', dedicatedServer=servername
                                       )
        else:
            print('Yep, there\'s an interface')
            result = self._client.post('/vrack/' + vrack + '/dedicatedServerInterface',
                                       dedicatedServerInterface=interface[0],
                                       )



        return result
