import requests
import json
import logging
from foreman.client import Foreman
from typing import Optional
from modules.module import RestModule


class ForemanClient(RestModule):
    _client = None

    def __init__(self, _settings):
        super().__init__(_settings)
        self.name = 'foreman'
        self._client = self._get_client(_settings)

    def _get_client(self, settings=None):
        if self._client is None and settings is not None:
            # self._client = Foreman(url=self._server, auth=(self._user, self._pass), api_version=2)
            user = settings['user']
            passwd = settings['pass']
            self._client = Foreman(settings['url'], (user, passwd), api_version=settings['api_version'])
            return self._client
        else:
            return self._client

    def _run(self, func, request_data):
        self._get_client()
        try:
            response = func(self, **json.loads(request_data))
            if len(response) == 0:
                return self._return_message('Nothing to see here, move along', False)
            else:
                return self._return_message(response, True)
        except Exception as e:
            return self._return_message(str(e))

    def _translator(self, response):
        print(response)
        return super()._return_message(response)

    def hostgroup_find(self, search_string: str=None):
        return self._client.hostgroups.index(search=search_string)

    def hostgroup_list(self):
        return self._client.hostgroups.index()

    def hostgroup_show(self, hostgroup: int=0):
        return self._client.hostgroups.show(id=hostgroup)

    def server_list(self):
        return self._client.hosts.index()

    def server_show(self, hostname: str=None):
        return self._client.hosts.show(id=hostname)

    def server_create(self, hostname: str=None, ip: str='0.0.0.0', mac: str='fa:fa:fa:fa:fa:fa', hostgroup: str=None, build: bool=True):
        host = {}

        host['name'] = hostname
        host['ip'] = ip
        host['mac'] = mac
        host['build'] = build

        search_results = self.hostgroup_find(hostgroup)
        for hg in search_results['results']:
            if hg['title'].lower() == hostgroup.lower():
                host['hostgroup_id'] = hg['id']
                break

        return self._client.hosts.create(host)

